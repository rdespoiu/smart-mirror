var messageTimeOfDay = "";

function updateClock ( )
{
  var currentTime = new Date ( );

  var currentHours = currentTime.getHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );
  var currentMonth = currentTime.getMonth ( ) + 1; //Because months are indexed from 0-11
  var currentDay = currentTime.getDate ( );
  var currentYear = currentTime.getFullYear ( );
  var dayOfWeek = currentTime.getDay();
  var daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  dayOfWeek = daysOfWeek[parseInt(dayOfWeek)];
  
  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

  var currentDateString = currentMonth + "/" + currentDay + "/" + currentYear;
  

  // Update the time display
  document.getElementById("clock").firstChild.nodeValue = currentTimeString;
  document.getElementById("date").firstChild.nodeValue = currentDateString;
  document.getElementById("day").firstChild.nodeValue = dayOfWeek;

  if (timeOfDay == "AM") {
    updateMessageForTimeOfDay("morning");
  }
  else if (timeOfDay == "PM") {
    if (currentHours < 5) {
      updateMessageForTimeOfDay("afternoon");
    }
    else if (currentHours >= 5) {
      updateMessageForTimeOfDay("evening");
    }
  }

}

function convertDate(dateFormat) {
  var monthTable = {"01": "January", "02": "February", "03": "March",
                    "04": "April", "05": "May", "06": "June", 
                    "07": "July", "08": "August", "09": "September",
                    "10": "October", "11": "November", "12": "December"};

  var outputString = "";

  var month = monthTable[dateFormat.substring(5,7)];
  var day = "";

  if (dateFormat.substring(8,9) == "0") {
    day = dateFormat.substring(9);
  } else {
    day = dateFormat.substring(8)
  }

  outputString = month + " " + day;

  return outputString;
}
