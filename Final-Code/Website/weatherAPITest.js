function getWeather() {

    var currentTemp;
    var feelsLike;
    var precipChance;
    var weatherDescription;

    $.ajax({

        url: "https://api.forecast.io/forecast/f399da1ab37d9a4c45c6cfc4af7026cc/42.0464,-87.6947",
        dataType: "jsonp",
        success: function(data) {
            console.log(data);
            console.log(data.currently.temperature); 

            currentTemp = data.currently.temperature;
            feelsLike = data.currently.apparentTemperature;
            precipChance = data.currently.precipProbability;
            precipType = data.currently.precipType;
            weatherDescription = data.currently.summary;

            console.log("Current Temperature: ", currentTemp);
            console.log("Feels Like: ", feelsLike);
            console.log("Chance of Precipitation: ", precipChance + "%");
            console.log("Type of precip: ", precipType);
            console.log("Description: ", weatherDescription);
            console.log("THIS WORKS YAY");

            if (precipType == null) {
                console.log("NULL PRECIP YAY")
            }

            var line1 = currentTemp + "&deg;" + " <img class = 'weatherIcon' src = 'thermometer.png'/>";
            //var line2 = "Feels Like: " + data.currently.apparentTemperature + "&deg;";

            var line3 = precipChance + "%" + " <img class = 'weatherIcon' src = 'rain.png'/>";

            var line4 = weatherDescription + " <img class = 'weatherIcon' src = 'sun.png'/>";

            document.getElementById("currentTemp").innerHTML = line1;
            //document.getElementById("feelsLikeTemp").innerHTML = line2;
            document.getElementById("chanceOfPrecip").innerHTML = line3;
            document.getElementById("weatherDescription").innerHTML = line4;

        }

    });

    
}

