import cv2
import config
import face
import select
import sys
import AuthenticateJSON
import RANGEFINDER
import time

def writeFile(filename, dataToWrite):
    with open('{}.txt'.format(filename), 'w+') as file:
        for item in dataToWrite:
            file.write(item)
    


def isLetterInput(letter):
	if select.select([sys.stdin,],[],[],0.0)[0]:
		inputChar = sys.stdin.read(1)
		return inputChar.lower() == letter.lower()
	return False

def scan():
    print("Looking for face...")

    image = camera.read()
    image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    result = face.detect_single(image)

    if result is None:
        print("Could not detect a single face. There may be other faces in view. Check the image in capture.pgm to see what was captured.")
        continue

    x, y, w, h = result
    crop = face.resize(face.crop(image, x, y, w, h))
    label, confidence = model.predict(crop)
            
    positiveId = ""

    if label == config.POSITIVE_LABEL:
        positiveId = "POSITIVE"
    else:
        positiveId = "NEGATIVE"

    print('Predicted {0} face with confidence {1} (Lower number is higher confidence).'.format(positiveId, confidence))

    if label == config.POSITIVE_LABEL and confidence < config.POSITIVE_THRESHOLD:
        AuthenticateJSON.writeToJSON(True)
        print('Face recognized. Access granted.')
        time.sleep(300) #If a user is successfully authenticated, the application will sleep for x minutes. That is the duration of the authenticated session. After that, the application will run again and re-determine authentication status
                    
    else:
        AuthenticateJSON.writeToJSON(False)
        print('Face was unrecognized. Access denied.')

def manualScan():
    while True:
        if isLetterInput('c'):
            scan()
 

if __name__ == '__main__':
    
    #Load training data for analysis
    print("Loading training data")
    model = cv2.createEigenFaceRecognizer()
    model.load(config.TRAINING_FILE)
    print("Training data has been loaded")
    
    camera = config.get_camera()
    
    print("Application is ready")
    print("Press button on iPhone or web application to scan face")
    print("Alternatively, type 'c' in the console and press enter to scan face")
    print("If using Range Finder, disregard above instructions")
    
    while True:

        print("Would you like to use the range finder to scan or manually by key input? 'r' for range finder, 'm' for manual")
        method = input('>  ')

        if method == 'r':
            distance = RANGEFINDER.getDistance()
            if distance <= 2: #Distance <= 2 meters
                scan()
            else:
                print("Nothing in range...")
        elif method == 'm':
            manualScan()

