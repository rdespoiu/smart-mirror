/*function getWeather() {
    $.get("http://api.openweathermap.org/data/2.5/weather?q=Evanston,us&units=imperial&appid=ad4df970a1359a3cb32c71b54273b538", function (response) {

        var rawJSON = JSON.stringify(response);

        var city = response.name;

        var currentTemp = response.main.temp;

        var minTemp = response.main.temp_min;

        var maxTemp = response.main.temp_max;

        var precip = response.rain.rain.3h;

        var weatherConditions = response.weather[0].description;

        console.log(rawJSON);
        console.log("City: ", city)
        console.log("Current Temperature: ", currentTemp);
        console.log("Low Temp Today: ", minTemp);
        console.log("High Temp Today: ", maxTemp);
        console.log("Current Weather Conditions: ", weatherConditions);

        text = '<b>Current Temperature: </b>' + response.main.temp + ' F<br/>';
        text += '<b>Weather Conditions: </b>' + response.weather[0].description + '<br/>';
        $('#parsed_json').append(text);

    });
}*/

/*function getWeather() {
    $.get("https://api.forecast.io/forecast/f399da1ab37d9a4c45c6cfc4af7026cc/42.0464,-87.6947", function (response) {

        var rawJSON = JSON.stringify(response);

        var currentTemp = response.currently.temperature;


        console.log(rawJSON);

        console.log("Current Temperature: ", currentTemp);


    });
}*/

function getWeather() {

    var currentTemp;
    var feelsLike;
    var precipChance;
    var weatherDescription;

    $.ajax({

        url: "https://api.forecast.io/forecast/f399da1ab37d9a4c45c6cfc4af7026cc/42.0464,-87.6947",
        dataType: "jsonp",
        success: function(data) {
            console.log(data);
            console.log(data.currently.temperature); 

            currentTemp = data.currently.temperature;
            feelsLike = data.currently.apparentTemperature;
            precipChance = data.currently.precipProbability;
            precipType = data.currently.precipType;
            weatherDescription = data.currently.summary;

            console.log("Current Temperature: ", currentTemp);
            console.log("Feels Like: ", feelsLike);
            console.log("Chance of Precipitation: ", precipChance + "%");
            console.log("Type of precip: ", precipType);
            console.log("Description: ", weatherDescription);
            console.log("THIS WORKS");

            if (precipType == null) {
                console.log("NULL PRECIP YAY")
            }

            var line1 = currentTemp + "&deg;" + " <img class = 'weatherIcon' src = 'thermometer.png'/>";
            //var line2 = "Feels Like: " + data.currently.apparentTemperature + "&deg;";

            var line3 = precipChance + "%" + " <img class = 'weatherIcon' src = 'rain.png'/>";

            var line4 = weatherDescription + " <img class = 'weatherIcon' src = 'sun.png'/>";

            document.getElementById("currentTemp").innerHTML = line1;
            //document.getElementById("feelsLikeTemp").innerHTML = line2;
            document.getElementById("chanceOfPrecip").innerHTML = line3;
            document.getElementById("weatherDescription").innerHTML = line4;

        }

    });

    
}

